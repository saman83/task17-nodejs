const readline = require("readline"); //Require reads file, its a global JS

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const message = `
Choose an option:
1. Read my package.json
2. Get my OS info
3. Start a HTTP Server
------------------------
Type a number: `;

console.log(message);

rl.question("", answer => {
  if (answer == 1) {
    const fs = require("fs");

    fs.readFile(__dirname + "/package.json", "utf-8", (err, contents) => {
      console.log("Error: ", err);

      console.log("File contents: ", contents);

      const rows = contents.split(",");

      console.log(rows);
    });
  } else if (answer == 2) {
    const os = require("os");
    console.log(
      "System Memory",
      (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + " GB"
    );
    console.log(
      "FREE Memory",
      (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + " GB"
    );
    console.log("CPU CORES", os.cpus().length);
    console.log("ARCH", (os.arch()));
    console.log("PLATFORM", (os.platform()));
    console.log("RELEASE", (os.release()));
    console.log("USER", (os.userInfo()));
  } else if (answer == 3) {
    const http = require("http");

    const server = http.createServer((req, res) => {
      if (req.url === "/") {
        res.write("Hello World!");
        res.end();
      }

      if (req.url === "/projects") {
        res.write(JSON.stringify(["JavaScript", "Node.JS"]));
        res.end();
      }
    });
    server.listen(3000);
    } else {
        console.log('Invalid option');
        }

//       });

//   });
  rl.close();
});
